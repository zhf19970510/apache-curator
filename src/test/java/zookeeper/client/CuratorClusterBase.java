package zookeeper.client;


import zookeeper.curator.CuratorStandaloneBase;

public  class CuratorClusterBase extends CuratorStandaloneBase {

    private final static  String CLUSTER_CONNECT_STR="192.168.199.136:2181,192.168.199.136:2182,192.168.199.136:2183,192.168.199.136:2184";

    public   String getConnectStr() {
        return CLUSTER_CONNECT_STR;
    }
}
