package com.zhf.distributedlock.entity;

/**
 * @author: 曾鸿发
 * @create: 2022-07-12 08:09
 * @description：
 **/
public class Order {

    private Integer id;
    private Integer pid;
    private String userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
