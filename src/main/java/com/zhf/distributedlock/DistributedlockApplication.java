package com.zhf.distributedlock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: 曾鸿发
 * @create: 2022-07-12 08:08
 * @description：启动类
 **/
@MapperScan("com.zhf.distributedlock.mapper")
@SpringBootApplication
public class DistributedlockApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributedlockApplication.class, args);
    }

}
