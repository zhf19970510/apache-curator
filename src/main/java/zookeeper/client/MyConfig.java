package zookeeper.client;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author: 曾鸿发
 * @create: 2022-07-10 13:26
 * @description：
 **/
@Data
@ToString
@NoArgsConstructor
public class MyConfig {
    private String key;
    private String name;

}
